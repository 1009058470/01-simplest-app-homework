package com.twuc.webApp.com;

public class calc {
    static public String calcString(String symbol) {
        StringBuffer ret = new StringBuffer();
        for(int i = 1; i <= 9; i++) {
            for(int j = 1; j <= 9; j++) {
                if(i < j) continue;
                if(i==7 && j==4)
                    ret.append("&nbsp;");
                if(symbol.equals("+")){
                    ret.append(i + symbol + j + "=" + (i+j));
                    if(i + j >= 10)
                        ret.append("&nbsp;");
                    else
                        ret.append("&nbsp;&nbsp;");
                }

                else if(symbol.equals("*")){
                    ret.append(i + symbol + j + "=" + (i*j));
                    if(i * j >= 10)
                        ret.append("&nbsp;");
                    else
                        ret.append("&nbsp;&nbsp;");
                }
            }
            ret.append("<br >");
        }
        return ret.toString();
    }
}
