package com.twuc.webApp.com;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.twuc.webApp.com.calc;

@RestController
public class Mult {
    @RequestMapping(value = "/api/tables/multiply")
    String mult(){
        return calc.calcString("*");
    }
}
