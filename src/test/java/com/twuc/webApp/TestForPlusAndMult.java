package com.twuc.webApp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
public class TestForPlusAndMult {
    @Autowired
    MockMvc moc;

    @Test
    public void test_table_plus_status_code() throws Exception {
        Assertions.assertEquals(moc.perform(get("/api/tables/plus")).andReturn().getResponse().getStatus(),200);
    }

    @Test
    public void test_table_plus_status_text_plain() throws Exception {
        // Assertions.assertEquals(moc.perform(get("/api/tables/plus")).andReturn().getResponse().getContentType(),"text/plain;charset=UTF-8");
        assert(moc.perform(get("/api/tables/plus")).andReturn().getResponse().getContentType().contains("text/plain"));
    }

    @Test
    public void test_table_multiply_status_code() throws Exception {
        Assertions.assertEquals(moc.perform(get("/api/tables/multiply")).andReturn().getResponse().getStatus(),200);
    }

    @Test
    public void test_multiply_text_plain() throws Exception {
        // Assertions.assertEquals(moc.perform(get("/api/tables/multiply")).andReturn().getResponse().getContentType(),"text/plain");
        assert(moc.perform(get("/api/tables/plus")).andReturn().getResponse().getContentType().contains("text/plain"));
    }
}
